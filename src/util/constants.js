import Vue from 'vue'
export default {install(Vue,options){
	const constObj = {
		$toggleTab:"door",
		$text:"",
		$presTab:"all",
		$projectId: "",
		$supplierId: "",
		$path:"HCHOO-PKG-1",
		$about:{
			address:{},
			area:{},
			situation:[],
			time:{},
			date:{},
			personnels:{},
			tional:[],
			note:{},
		},
		$why:[],
		$whyText:"",
		$date:{},
		$statusicon:{
			unpaid: false,
			inprocess: false,
			needReview: false
		},
        settoggleTab: val=>{
			Vue.prototype.$toggleTab = val;
		},
		setText:val=>{
			Vue.prototype.$text = val;
		},
		setPresTab:val=>{
			Vue.prototype.$presTab = val;
		},
		setAbout:(key,val)=>{
			Vue.prototype.$about[key] = val;
		},
		setAboutAll:(val)=>{
			Vue.prototype.$about = val;
		},
		setProjectId: val=>{
			Vue.prototype.$projectId = val;
		},
		setSupplierId: val => {
			Vue.prototype.$supplierId = val;
		},
		setPath:(val)=>{
			Vue.prototype.$path = val;			
		},
		setStatusicon: (key,val) => {
			Vue.prototype.$statusicon[key] = val;
		},
		setWhy:(val)=>{
			Vue.prototype.$why = val;						
		},
		setWhyText:(val)=>{
			Vue.prototype.$whyText = val;			
		},
		setDate:(val)=>{
			Vue.prototype.$date = val;			
		},
	};
	Object.assign(Vue.prototype,constObj);
}}