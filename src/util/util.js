

// var baseUrl = "http://test.hellotoby.com:8888/";//测试服
// var pageUrl = "http://test.hellotoby.com/";
var baseUrl = "http://alipaytest.hellotoby.com/";
// var baseUrl = "https://api.hellotoby.com";//正式服

import Ajax from './ajax'

export const ajax = (params) => {
	var loading = String(params.loading) == "undefined" ?true:params.loading;

	var defaults = {
		url: baseUrl + params.url,
		dataType:params.dataType? params.dataType:"json",
		contentType:params.contentType?params.contentType:"application/x-www-form-urlencoded",
		type: params.type ? params.type : "post",
		async: String(params.async) ? params.async : true,
	};

	if(params.urlType){
		defaults.url =  params.url;
	}

	if(loading){
		myApp.$root.eventHub.$emit('loading', true)
	}


	if(params.type === "get"){
		
	}
	if(params.type === "post"){
		params.data = JSON.stringify(params.data);
		defaults.dataType = "json";
        defaults.contentType = "application/json;charset=UTF-8";
	}

	for(var key in defaults) {
		params[key] = defaults[key];
	}
	var _successFn = params.success;
	var _error = params.error;
	params.beforeSend = function(req) {
		// console.log(req)
	}
	params.success = function(result, status, xhr) {
		// alert(JSON.stringify(result))
		// console.log(result)
		if(loading){
			myApp.$root.eventHub.$emit('loading', false);					
		}
		if(result.success) {
			_successFn(result, status, xhr)
		} else {
			if(_error){
				_error(result);		
			}else{
				myApp.$root.eventHub.$emit("showprompt",{
					content:result.errorMessage,
				});
			}
		}
	}
	params.error = function(err) {
		if(loading){
			myApp.$root.eventHub.$emit('loading', false);					
		}
		myApp.$root.eventHub.$emit("showprompt",{
			content:err?err:"请求超时！",
		});
	}
	Ajax(params);
}

export const dateFormat = (tmp, fmt) => {
	var date = null;
	if(!tmp) {
		return "";
	}
	if(tmp instanceof Date) {
		date = tmp;
	} else if(typeof tmp == "string" || typeof tmp == "number") {
		date = new Date(tmp);
	} else {
		return "";
	}
	if(date == "Invalid Date") {
		var aaa = tmp.replace(/-/g, "/");
		date = new Date(aaa);
	}
	if(!fmt) {
		fmt = "yyyy-MM-dd";
	}
	if(/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	var o = {
		"M+": date.getMonth() + 1, //月份
		"d+": date.getDate(), //日
		"h+": date.getHours(), //小时
		"m+": date.getMinutes(), //分
		"s+": date.getSeconds(), //秒
		"q+": Math.floor((date.getMonth() + 3) / 3), //季度
		"S": date.getMilliseconds() //毫秒
	};
	for(var k in o) {
		if(new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
	  // 'yyyy-MM-dd hh:mm:ss'
	  //'yyyy年MM月dd日'

}

//设置cookie
export const setCookie = (cname, cvalue, exdays) => {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
	// console.log(document.cookie);
  }
  //获取cookie
export const  getCookie = (cname) => {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') c = c.substring(1);
	  if (c.indexOf(name) != -1){
		return c.substring(name.length, c.length);
	  }
	}
	return "";
  }
  //清除cookie
export const  clearCookie = () =>{
	this.setCookie("username", "", -1);
}
// 遍历questions请求数据
export const  getQuestions = (data,num) =>{
	var datas = {
		questions:[],
	}
	if(typeof num === "number"){
		datas.pageNum = num
	}else{
		datas.packageNumber = num
	}
	if(JSON.stringify(data.address) != "{}"){
		var arry = {
			questionNumber:data.address.questionNumber,
			answers:[{
				value:data.address.id,
				label:data.address.address
			}]
		}
		datas.questions.push(arry);
	}
	if(JSON.stringify(data.area) != "{}"){
		var arry = {
			questionNumber:data.area.questionNumber,
			answers:[{
				value:data.area.value
			}]
		}
		datas.questions.push(arry);		
	}

	if(JSON.stringify(data.situation) != "[]"){
		var arry = {
			questionNumber:data.situation[0].questionNumber,
			answers:[],
		}
		for(var i=0; i<data.situation.length; i++){
			arry.answers.push({value:data.situation[i].value});
		}
		datas.questions.push(arry);
	}
	console.log(data.time)
	if(JSON.stringify(data.time) != "{}"){
		var arry = {
			questionNumber:data.time.questionNumber,
			answers:[{
				value:data.time.initValue
			}]
		}
		datas.questions.push(arry);		
	}
	if(JSON.stringify(data.date) != "{}"){
		var arry = {
			questionNumber:data.date.questionNumber,
			answers:[{
				value:data.date.day+"  "+data.date.startTime
			}]
		}
		datas.questions.push(arry);		
	}
	if(JSON.stringify(data.tional) != "[]"){
		var arry = {
			questionNumber:data.tional[0].questionNumber,
			answers:[],
		}
		for(var i=0; i<data.tional.length; i++){
			arry.answers.push({value:data.tional[i].value});
		}
		datas.questions.push(arry);
	}
	if(JSON.stringify(data.note) != "{}"){
		var arry = {
			questionNumber:data.note.questionNumber,
			answers:[{
				value:data.note.value
			}]
		}
		datas.questions.push(arry);		
	}
	if(JSON.stringify(data.personnels) != "{}"){
		var arry = {
			questionNumber:data.personnels.questionNumber,
			answers:[{
				value:data.personnels.value
			}]
		}
		datas.questions.push(arry);		
	}
	return datas;

}
// 单独获取questions请求数据
export const  getQuestionsItem = (data,num,key) =>{
	console.log(data)
	var datas = {
		pageNum:num,
		questions:[],
	}
	if(JSON.stringify(data[key]) != "{}"){
		var arry = {
			questionNumber:data[key].questionNumber,
			answers:[{
				value:data[key].initValue?data[key].initValue:data[key].answers[0].value,
			}]
		}
		datas.questions.push(arry);		
	}
	// console.log(datas)
	return datas;
}
// 获取Monday
export const getMonday = (time,start) => {
	const dayList = myApp.$t("message.dayList");	
	const str = time.replace(/-/g, '/');
	const date = new Date(str).getTime();
	if(start){
		const today = new Date(new Date(new Date().toLocaleDateString()).getTime()+24*60*60*1000-1);
		const tomorrow = new Date(new Date(new Date().toLocaleDateString()).getTime()+48*60*60*1000-1);
		if(today > date){
			return dayList[7];
		}else if(tomorrow > date){
			return dayList[8];
		}else{
			const day = new Date(str).getDay();	
			return dayList[day];		
		}
	}else{
		const day = new Date(str).getDay();	
		return dayList[day];
	}
	
}
