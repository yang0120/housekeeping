module.exports={
    message: {
      loading:"Loading",

      head:{
          index:"Door to door service",
          fanhui:"Back",
          pre:"My reservation",
          detail:"Booking details",
          people:"Service personnel",
          process:"General process",
          cancel:"Cancellation",
          modify:"Change reservation",
          time:"Service time",
          ment:"comment",
          about:"make appointment",
          address:"Service address",
          aboutDetail:"Booking details",
          complete:"complete booking",
          record:"Reservation record",
      },
      // tab:{
      //   index:"Service center",
      //   pre:"My reservation",
      // },
      
      index:{
          fuwu:"Household",
          wxanz:"Maintenance",
          xiyi:"Laundry drying",
          mei:"Beauty beautiful",
      },
      pre:{
        all:"All",
        payment:"Payment",
        ongoing:"Ongoing",
        evaluate:"Evaluate",
      },
      prompt:{
          title:"Prompt",
      }, 
      btn:{
        mine:"Determine",
        next:"The next step",
        submit:"Submit",
        make:"Determined to make",
        bank:"Return to my service",
      },
      about:{
          addre:"Service address",
          area:"Household size",
          situation:"Household situation",
          usedAddre:"Commonly used address",
          newAddre:"The new address",
          num:"Service hours",
          hours:"hours",
          time:"Service time",
          detail:"The price detail",
          date:"Service date",
          people:"Service personnel",
          additional:"Additional charges",
          note:"Note",
          prompt:"Warm prompt",
          promptList:[
                      "If it is necessary to clean the air-conditioner dust screen, air extraction fan and smoke extraction machine, please remove it by yourself, so that the service personnel can clean it.",
                      "If there is any matter outside the general cleaning process that needs to be handled by the sister, it must be raised in the remarks, otherwise the sister has the right not to accept it."
                    ],
          tiaokuan:"Terms and conditions",
          tiaokuanList:[
            {
              content:"In order to guarantee the time benefit of service personnel, if the reservation is cancelled within 24 hours of arrival, the platform will not refund."
            },
            {
              content:"Confirm payment by pressing and agree on my behalf",
              text:"Privacy regulations",
            }
          ]
      },
      
      input:{
        name:"The contact",
        phone:"Mobile phone",
        addre:"Service address",
        detailsAddre:"Details about the address",
      },
      placeholder:{
        name:"Please enter contact",
        phone:"Please enter your mobile phone number",
        addre:"Please enter service address",
        detailsAddre:"Please enter service address",
        date:"Please select service period",
        special:"Please fill in the special needs here, we will try our best to arrange.",
        ment:"Is the service up to your expectations? Please share your experience of using up to 100 words.",
      },

      advice:{
        title:"Recommended service hours",
        content:"The recommended service hours are to calculate the most suitable service hours based on the area of the home and the situation of the home. Of course, you can also make an appointment with the lowest service hours, but it may be affected by the lack of time.",
        text:"General cleaning process",
      },
      order:{
        has:"Payment has been",
        people:"Dispatched personnel",
        peopleBtn:"Check the staff",
        complete:"Service complete",
        again:"Once again",
        imme:"Evaluation",
        increase:"Additional cost",
        payment:"Payment",
        have:"Have evaluation",
        cancelled:"Has cancelled",
        text:"Evaluation will be a bonus, next appointment will reduce you $10!",
        modify:"Change reservation",
        cancel:"A cancellation",
        customer:"Contact clothing",
        complaints:"Complaints suit",
        record:"Reservation record",        
      },

      price:{
        subtotal:"Subtotal",
        stay:"For the payment"
      },

      complete:{
        title:"Thank you for your appointment",
        text:"We will immediately match you with the appropriate service staff. The pairing time is usually no more than 1 hour. Please wait patiently.",
        btn:"Return to my service",
      },

      ment:{
        title:{
          zhong:"The total evaluation",
          ping:"Service evaluation",
        },
        theme:{
          speed:"Speed",
          clean:"Clean",
          attitude:"Attitude",
        },
        score:{
          not:"Not satisfied with",
          general:"General",
          satisfied:"Satisfied with the",
          very:"Very satisfied with",
        },
        highpraise:{
          titel:"Thank you for your valuable advice",
          contents:"hank you for your use of our service and submitted service evaluation, we will do better for you!",
        },
        badreview:{
          titel:"Sorry about your experience",
          contents:"Thank you for your use of our service, sorry for your dissatisfaction in the service process, the customer service will contact you as soon as possible to learn more."
        }
      },
      dayList:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Today","Tomorrow"],      
      
      personnel:{
        systems:"The system recommends service personnel",
        onces:"Service personnel who have made reservations in the past",
        service:"service",
        times:"times",
        callser:"Call service staff",
        usercomt:"User comments",
        hours:"3 hours"
      },

      presdeta:{
        changeday:"Change the appointment to today",
        needpay:"Need to pay",
        hasreful:"Payment has been",
        termspay:"Terms of payment",
        timepay:"Time of payment",
        orderpay:"Payment order no",
        status:{
          quoting:"In the match",
          hired:"Matched service personnel",
          done:"Service completed, to be evaluated",
          cancelled:"Appointment cancelled"
        },
        payment:{
          alipay:"Pay treasure",
          stripe:"Qr code",
          cash:"cash",
          bank:"Bank card",
          other:"Other payment"
        }
      },

      modify:{
        timearr:"Time of arrival",
      }
    }
  }